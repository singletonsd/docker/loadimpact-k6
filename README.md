# Singleton SD - Loadimpact - k6

Creates a docker image of loadimpact/k6 setting the entry point to default in order to use it inside jenkins.

## Documentation

[Github Repository](https://github.com/loadimpact/k6)

[Dockerhub](https://hub.docker.com/r/loadimpact/k6/)

----------------------

© [Singleton](http://singletonsd.com), Italy, 2019.
